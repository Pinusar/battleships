import models.BattleShip;
import models.Board;
import models.Cell;
import models.Player;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class GameTests {
    @Test
    public void basicShipPlacementTest() {
        Game game = createBasicGame();
        Player player1 = game.getPlayer1();

        Cell startingCell = new Cell(1, 1);
        Cell endingCell = new Cell(5, 1);
        BattleShip battleShip = new BattleShip(startingCell, endingCell);
        game.placeShip(player1, battleShip);
        String expectedResult =
                "..........\n" +
                ".OOOOO....\n" +
                "..........\n" +
                "..........\n" +
                "..........\n";
        assert player1.getBoard().toString().equals(expectedResult);
    }

    @Test
    public void shipPlacementFailsWhenPlacingOutOfBoundsTest() {
        Game game = createBasicGame();
        Player player1 = game.getPlayer1();

        Cell startingCell = new Cell(1, 1);
        Cell endingCell = new Cell(17, 1);
        BattleShip battleShip = new BattleShip(startingCell, endingCell);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {game.placeShip(player1, battleShip);});
    }

    @Test
    public void shipPlacementFailsWhenOverlappingTest() {
        Game game = createBasicGame();
        Player player1 = game.getPlayer1();

        Cell startingCell = new Cell(1, 1);
        Cell endingCell = new Cell(5, 1);
        BattleShip battleShip = new BattleShip(startingCell, endingCell);
        game.placeShip(player1, battleShip);

        Cell startingCell2 = new Cell(2, 1);
        Cell endingCell2 = new Cell(2, 3);
        BattleShip battleShip2 = new BattleShip(startingCell2, endingCell2);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {game.placeShip(player1, battleShip2);});
    }

    public Game createBasicGame() {
        Board board = new Board(10, 5);
        Player player1 = new Player("Player 1", board, 5);
        Player player2 = new Player("Player 2", board, 5);
        Map<Integer, Integer> shipsPerPlayer = new HashMap<>();
        shipsPerPlayer.put(5, 1);
        return new Game(player1, player2, shipsPerPlayer);
    }
}
