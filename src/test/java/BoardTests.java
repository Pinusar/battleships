import models.Board;
import org.junit.Test;

public class BoardTests {
    @Test
    public void basicBoardCreationTest() {
        Board board = new Board(10, 10);
        String expectedResult =
                "..........\n" +
                "..........\n" +
                "..........\n" +
                "..........\n" +
                "..........\n" +
                "..........\n" +
                "..........\n" +
                "..........\n" +
                "..........\n" +
                "..........\n";
        assert board.toString().equals(expectedResult);
    }
}
