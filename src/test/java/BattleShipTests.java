import models.BattleShip;
import models.Cell;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class BattleShipTests {
    @Test
    public void shipCreationFailsWhenShipIsNotStraight() {
        Cell startingCell = new Cell(2, 2);
        Cell endingCell = new Cell(5, 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            BattleShip battleShip = new BattleShip(startingCell, endingCell);});
    }
}
