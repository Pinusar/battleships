import models.BattleShip;
import models.Board;
import models.Cell;
import models.Player;

import java.util.*;

public class Game {
    protected Player player1;
    protected Player player2;
    protected List<Player> players;
    protected boolean isOver;
    protected Map<Integer, Integer> battleshipsPerPlayer;

    public Game(Player player1, Player player2, Map<Integer, Integer> battleshipsPerPlayer) {
        this.player1 = player1;
        this.player2 = player2;
        this.battleshipsPerPlayer = battleshipsPerPlayer;
        this.isOver = false;
        this.players = new ArrayList<>();
        this.players.add(player1);
        this.players.add(player2);
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public boolean isOver() {
        return isOver;
    }

    public Map<Integer, Integer> getBattleshipsPerPlayer() {
        return battleshipsPerPlayer;
    }

    public void placeShip(Player placingPlayer, BattleShip battleShip) {
        char[][] boardContent = placingPlayer.getBoard().getContent();
        Cell startingCell = battleShip.getStartingCell();
        Cell endingCell = battleShip.getEndingCell();
        validatePlacingCells(placingPlayer, startingCell, endingCell);
        if (startingCell.getX() == endingCell.getX()) {
            for (int y = startingCell.getY(); y < endingCell.getY() + 1; y ++) {
                boardContent[y][startingCell.getX()] = 'O';
            }
        } else if (startingCell.getY() == endingCell.getY()) {
            for (int x = startingCell.getX(); x < endingCell.getX() + 1; x ++) {
                boardContent[startingCell.getY()][x] = 'O';
            }
        } else {
            throw new IllegalArgumentException("Ships must be placed vertically or horizontally");
        }
    };

    public void validatePlacingCells(Player placingPlayer, Cell startingCell, Cell endingCell) {
        Set<Cell> cells = new HashSet<>();
        cells.add(startingCell);
        cells.add(endingCell);
        Board board = placingPlayer.getBoard();
        validateCellBounds(board, cells);
        char[][] boardContent = board.getContent();
        checkForOverLap(startingCell, endingCell, boardContent);
    }

    public void validateCellBounds(Board board, Set<Cell> cells) {
        for (Cell cell: cells
        ) {
            if (cell.getX() < 0 ||
                    cell.getY() < 0 ||
                    cell.getX() > board.getWidth() - 1 ||
                    cell.getY() > board.getHeight() -1) {
                throw new IllegalArgumentException("Cells are out of bounds for placing ship!");
            }
        }
    }

    public void checkForOverLap(Cell startingCell, Cell endingCell, char[][] boardContent) {
        if (startingCell.getX() == endingCell.getX()) {
            for (int y = startingCell.getY(); y < endingCell.getY() + 1; y ++) {
                if (boardContent[y][startingCell.getX()] == 'O') {
                    throw new IllegalArgumentException("The cell already contains a ship!");
                }
            }
        } else if (startingCell.getY() == endingCell.getY()) {
            for (int x = startingCell.getX(); x < endingCell.getX() + 1; x ++) {
                if (boardContent[startingCell.getY()][x] == 'O') {
                    throw new IllegalArgumentException("The cell already contains a ship!");
                }
            }
        }
    }




}
