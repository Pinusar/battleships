import models.Board;
import models.Player;

import java.util.HashMap;
import java.util.Map;

public class ConsoleApp {
    public static void main(String[] args) {
        Board board = new Board(20, 20);
        Player player1 = new Player("Player 1", board, 5);
        Player player2 = new Player("Player 2", board, 5);
        Map<Integer, Integer> shipsPerPlayer = new HashMap<>();
        shipsPerPlayer.put(5, 1);
        shipsPerPlayer.put(4, 2);
//        shipsPerPlayer.put(3, 3);
//        shipsPerPlayer.put(2, 4);
        ConsoleGame game = new ConsoleGame(player1, player2, shipsPerPlayer);
        game.play();
    }
}
