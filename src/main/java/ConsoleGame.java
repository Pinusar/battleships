import models.BattleShip;
import models.Cell;
import models.Player;

import java.util.Map;
import java.util.Scanner;

public class ConsoleGame extends Game {
    public ConsoleGame(Player player1, Player player2, Map<Integer, Integer> battleshipsPerPlayer) {
        super(player1, player2, battleshipsPerPlayer);
    }

    public Cell askForCell() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] parts = input.split(" ");
        int x = Integer.parseInt(parts[0]);
        int y = Integer.parseInt(parts[1]);
        return new Cell(x, y);
    }

    public void placeAllShips(Player placingPlayer) {
        for (Map.Entry<Integer, Integer> entry: this.battleshipsPerPlayer.entrySet()
             ) {
            int shipSize = entry.getKey();
            for (int i = 0; i < entry.getValue(); i++) {
                System.out.println("Placing battleship with size " + shipSize);
                System.out.println("Please select starting cell:");
                Cell startingCell = askForCell();
                System.out.println("Please select ending cell:");
                Cell endingCell = askForCell();
                System.out.println("Building battleship");
                BattleShip battleShip = new BattleShip(startingCell, endingCell);
                System.out.println("Deploying battleship");
                placeShip(placingPlayer, battleShip);
                System.out.println("Battleship placed!");
                System.out.println(placingPlayer.getBoard());
            }
        }
        System.out.println("All battleships deployed!");
        System.out.println(placingPlayer.getBoard());
    }

    public void initializeBoards() {
        for (Player player: players
        ) {
            System.out.println("Placing ships for player: " + player.getName());
            placeAllShips(player);
        }
    }

    public void play() {
        initializeBoards();
        while (!isOver) {
            playTurn();
        }
    }

    public Player getTargetPlayer(Player activePlayer) {
        Player targetPlayer;
        if (activePlayer.equals(player1)) {
            targetPlayer = player2;
        } else {
            targetPlayer = player1;
        }
        return targetPlayer;
    }

    public boolean guessHits(Player targetPlayer, Cell cell) {
        int x = cell.getX();
        int y = cell.getY();
        return targetPlayer.getBoard().getContent()[x][y] == 'O';
    }

    public void markCellAsHit(Player targetPlayer, Cell cell) {
        targetPlayer.getBoard().getContent()[cell.getX()][cell.getY()] = 'X';
    }

    public void reduceLives(Player targetPlayer) {
        int currentLives = targetPlayer.getLivesLeft();
        targetPlayer.setLivesLeft(currentLives - 1);
    }

    public void playTurn() {
        for (Player activePlayer: players
        ) {
            Player targetPlayer = getTargetPlayer(activePlayer);
            System.out.println("Please enter a cell to bombard:");
            Cell targetCell = askForCell();
            if (guessHits(targetPlayer, targetCell)) {
                System.out.println("HIT!");
                markCellAsHit(targetPlayer, targetCell);
                reduceLives(targetPlayer);
                System.out.println(targetPlayer.getBoard());
                if (hasPlayerLost(targetPlayer)) {
                    isOver = true;
                    System.out.println("Game over!");
                }
            } else {
                System.out.println("MISS!");
            }
        }
    }

    public boolean hasPlayerLost(Player player) {
        return player.getLivesLeft() < 1;
    }
}
