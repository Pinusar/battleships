package models;

import java.util.Set;

public class Board {
    private int width;
    private int height;
    private Set<BattleShip> battleShips;
    private char[][] content;
    /*
    O ship
    X destroyed ship
    . empty cell
     */

    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        this.content = new char[height][width];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                this.content[y][x] = '.';
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public char[][] getContent() {
        return content;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                result.append(this.content[y][x]);
            }
            result.append('\n');
        }
        return result.toString();
    }
}
