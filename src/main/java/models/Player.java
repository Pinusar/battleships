package models;

public class Player {
    private String name;
    private Board board;
    private int livesLeft;

    public Player(String name, Board board, int livesLeft) {
        this.name = name;
        this.board = board;
        this.livesLeft = livesLeft;
    }

    public String getName() {
        return name;
    }

    public Board getBoard() {
        return board;
    }

    public int getLivesLeft() {
        return livesLeft;
    }

    public void setLivesLeft(int livesLeft) {
        this.livesLeft = livesLeft;
    }
}
