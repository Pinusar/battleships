package models;

public class BattleShip {
    private int size;
    private Cell startingCell;
    private Cell endingCell;

    public BattleShip(Cell startingCell, Cell endingCell) {
        if (startingCell.getX() == endingCell.getX()) {
            this.size = endingCell.getY() - startingCell.getY() + 1;
        } else if (startingCell.getY() == endingCell.getY()) {
            this.size = endingCell.getX() - startingCell.getX() + 1;
        } else {
            throw new IllegalArgumentException("Ships must be placed vertically or horizontally");
        }
        this.startingCell = startingCell;
        this.endingCell = endingCell;
    }

    public int getSize() {
        return size;
    }

    public Cell getStartingCell() {
        return startingCell;
    }

    public Cell getEndingCell() {
        return endingCell;
    }
}
